﻿namespace OpenWeatherProvider
{
    public class SystemInfo
    {
        public int Id { get; set; }

        public int Type { get; set; }

        public string Message { get; set; }

        public string Country { get; set; }

        public long Sunrise { get; set; }

        public long Sunset { get; set; }
    }
}