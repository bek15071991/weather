﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json; 

namespace OpenWeatherProvider
{
    public class OpenWeatherConfiguration
    {
        public Uri Host { get; set; }

        public string AppId { get; set; }

        public string City { get; set; }
    }

    public class OpenWeatherHttpClient
    {
        private readonly OpenWeatherConfiguration _configuration; 

        public OpenWeatherHttpClient(OpenWeatherConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task<WeatherResponse> Get()
        {
            using (var httpClient = new HttpClient())
            {
                var url =
                    $"{_configuration.Host}/data/2.5/weather?q={_configuration.City}&appid={_configuration.AppId}";

                var result = await httpClient.GetAsync(url).ConfigureAwait(false);

                var contentResult = await result.Content.ReadAsStringAsync().ConfigureAwait(false);

                return JsonConvert.DeserializeObject<WeatherResponse>(contentResult);
            }
        }
    }
}