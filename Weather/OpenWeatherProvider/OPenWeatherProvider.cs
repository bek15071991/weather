﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Weather.Core;

namespace OpenWeatherProvider
{
    public class OpenWeatherProvider : IWeatherProvider
    {
        private readonly OpenWeatherConfiguration _configuration;


        public OpenWeatherProvider(OpenWeatherConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<WeatherInfo> ToDay()
        {
            var client = new OpenWeatherHttpClient(_configuration);

            var weatherResponse = await client.Get().ConfigureAwait(false);

            if (weatherResponse == null)
                throw new InvalidOperationException("Cannot get weathers info");

            var weather = weatherResponse.Weathers?.FirstOrDefault();

            if (weather == null)
                throw new InvalidOperationException("Weathers info is empty");

            var systemInfo = weatherResponse.SystemInfo;

            if (systemInfo == null)
                throw new InvalidOperationException("Systems info is empty");

            var cloud = weatherResponse.Cloud;

            if (cloud == null)
                throw new InvalidOperationException("Clouds info is empty");

            var wind = weatherResponse.Wind;

            if (wind == null)
                throw new InvalidOperationException("Wind info is empty");

            var weatherMain = weatherResponse.Main;

            if (weatherMain == null)
                throw new InvalidOperationException("WeatherMain info is empty");


            var sunrise = DateTime.FromFileTimeUtc(systemInfo.Sunrise);
            var sunset = DateTime.FromFileTimeUtc(systemInfo.Sunset);

            var windInfo = new WindInfo(wind.Speed, wind.Deg);

            var weatherMainInfo = new WeatherMainInfo(weatherMain.Temp, weatherMain.Pressure, weatherMain.Humidity,
                weatherMain.TempMin, weatherMain.TempMax);

            return new WeatherInfo(
                weatherResponse.Name,
                weather.Description,
                weatherResponse.Visibility,
                cloud.All,
                sunrise,
                sunset,
                windInfo,
                weatherMainInfo); 
        }
    }
}