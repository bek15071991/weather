﻿using Newtonsoft.Json;

namespace OpenWeatherProvider
{
    public class WeatherMain
    {
        public float Temp { get; set; }

        public int Pressure { get; set; }

        public int Humidity { get; set; }

        [JsonProperty("temp_min")]
        public float TempMin { get; set; }

        [JsonProperty("temp_max")]
        public float TempMax { get; set; }
    }
}