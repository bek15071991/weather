﻿namespace OpenWeatherProvider
{
    public class Coordinate
    {
        public double Lon { get; set; }

        public double Lat { get; set; }
    }
}