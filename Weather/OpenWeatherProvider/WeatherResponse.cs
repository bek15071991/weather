﻿using Newtonsoft.Json;

namespace OpenWeatherProvider
{
    public class WeatherResponse
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Cod { get; set; }

        public string Base { get; set; }

        public int Visibility { get; set; }

        [JsonProperty("dt")]
        public int DatetimeCalculation { get; set; }

        public WeatherMain Main { get; set; }

        public Wind Wind { get; set; } 

        [JsonProperty("clouds")]
        public Cloud Cloud { get; set; }

        [JsonProperty("sys")]
        public SystemInfo SystemInfo { get; set; } 

        [JsonProperty("weather")]
        public Weather[] Weathers { get; set; }
    }

    public class Cloud
    {
        public int All { get; set; }
    }
}
