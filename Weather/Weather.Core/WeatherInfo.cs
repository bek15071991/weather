﻿using System;

namespace Weather.Core
{
    public class WeatherInfo
    {
        public string City { get; }

        public string Description { get; }

        public int Visibility { get; }

        public int Cloud { get; }

        public DateTime Sunrise { get; }

        public DateTime Sunset { get; }

        public WindInfo Wind { get; }

        public WeatherMainInfo Main { get; }

        public WeatherInfo(string city,
            string description,
            int visibility,
            int cloud,
            DateTime sunrise,
            DateTime sunset,
            WindInfo wind,
            WeatherMainInfo main)
        {
            City = city;
            Description = description;
            Visibility = visibility;
            Cloud = cloud;
            Sunrise = sunrise;
            Sunset = sunset;
            Wind = wind;
            Main = main;
        }
    }
}