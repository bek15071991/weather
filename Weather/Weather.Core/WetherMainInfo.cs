﻿namespace Weather.Core
{
   public class WeatherMainInfo
    {
        public float Temperature { get;   }

        public float Pressure { get;   }

        public float Humidity { get;   }

        public float MinTemperature { get;   }

        public float MaxTemperature { get;   }

        public WeatherMainInfo(float temperature, float pressure, float humidity, float minTemperature, float maxTemperature)
        {
            Temperature = temperature;
            Pressure = pressure;
            Humidity = humidity;
            MinTemperature = minTemperature;
            MaxTemperature = maxTemperature;
        }
    }
}
