﻿namespace Weather.Core
{
    public class WindInfo
    {
        public float Speed { get;   }

        public float Deg { get;  }

        public WindInfo(float speed, float deg)
        {
            Speed = speed;
            Deg = deg;
        }
    }
}