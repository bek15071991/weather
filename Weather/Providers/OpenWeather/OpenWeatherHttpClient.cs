﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpenWeather
{
    public class OpenWeatherConfiguration
    {
        public Uri Address { get; set; }
    }

    public class OpenWeatherHttpClient
    {
        private readonly OpenWeatherConfiguration _configuration;

        public OpenWeatherHttpClient(OpenWeatherConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task<WeatherResponse> Get()
        {
            using (var httpClient = new HttpClient())
            {
                var result = await httpClient.GetAsync(_configuration.Address);

                var contentResult = await result.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<WeatherResponse>(contentResult);
            }
        }
    }
}