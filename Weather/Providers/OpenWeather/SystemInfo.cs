﻿namespace OpenWeather
{
    public class SystemInfo
    {
        public int Id { get; set; }

        public int Type { get; set; }

        public string Message { get; set; }

        public string Country { get; set; }

        public int Sunrise { get; set; }

        public int Sunset { get; set; }
    }
}