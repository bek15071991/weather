﻿namespace OpenWeather
{
    public class WeatherResponse
    {
        public int Id { get; set; }

        public string CityName { get; set; }

        public int Cod { get; set; }

        public string Base { get; set; }

        public int Visibility { get; set; }

        public int DataCalculationTime { get; set; } 

        public WeatherMain Main { get; set; }

        public Wind Wind { get; set; }

        public Weather[] Weathers { get; set; }

        public Cloud[] Clouds { get; set; }

        public SystemInfo[] SystemInfos { get; set; }
    }

    public class Cloud
    {
        public string All { get; set; }
    }
}
