﻿namespace OpenWeather
{
    public class WeatherMain
    {
        public float Temp { get; set; }

        public int Pressure { get; set; }

        public int Humidity { get; set; }

        public float TempMin { get; set; }

        public float TempMax { get; set; }
    }
}