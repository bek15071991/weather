﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Weather.Core;
using Weather.Web.Models;

namespace Weather.Web.Controllers
{
    public class WeatherController : Controller
    {
        private readonly IWeatherProvider _weatherProvider;
        private readonly ILogger<WeatherController> _logger;

        public WeatherController(IWeatherProvider weatherProvider, ILoggerFactory loggerFactory)
        {
            _weatherProvider = weatherProvider ?? throw new ArgumentNullException(nameof(weatherProvider));
            _logger = loggerFactory.CreateLogger<WeatherController>();
        }

        public async Task<IActionResult> ToDay()
        {
            try
            {
                var weatherInfo = await _weatherProvider.ToDay();

                return View(weatherInfo);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e, "Cannot get weather from provider");

                return RedirectToAction("Error");
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}